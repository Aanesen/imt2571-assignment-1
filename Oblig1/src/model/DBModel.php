<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db){
			$this->db = $db;
		}
    else
    {
      try{
          // Create PDO connection
          $db = new PDO('mysql:host=localhost;dbname=test;charset=utf8mb4', 'root', '');
          $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          //Sets the class's $db to the PDO $db :)
          $this->db = $db;
        }catch(PDOException $ex){
          echo "Could not connect to database"; //Error message
        }
      }
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
      try{
        //SQL Injection SAFE query method:
        $query = "SELECT * FROM book";
        $param = array();
        $stmt = $this->db->prepare($query);
        $stmt->execute($param);

        //Initializes $booklist as an Array();
		    $booklist = array();

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          //Fills $booklist with Book objects and data from SQL
          $booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
        }

      }catch(PDOException $ex){
        echo "Could not retrieve book list"; //Error message
      }
      return $booklist; //returns $booklist array with Book objects
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id){
      if(filter_var($id, FILTER_VALIDATE_INT) && $id > 0){
        try{
          $book = null; //initalizes variable

          //SQL Injection SAFE query method:
          $query = "SELECT * FROM book WHERE id = ?";
          $param = array($id);
          $stmt = $this->db->prepare($query);
          $stmt->execute($param);
          $row = $stmt->fetch(PDO::FETCH_ASSOC);
          if($row){
            //Constructs object Book, with data fetched from SQL
            $book = new Book($row['title'], $row['author'], $row['description'], $row['id']);
          }
        }catch(PDOException $ex){
          echo "Could not retrieve book"; //Error message
        }
          return $book; //returns $book object
      }
    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book){
      try{
        // Title and author cannot be left empty!";
        if($book->title != "" && $book->author != "" && $book->title != NULL && $book->author != NULL){

        //if empty, set as NULL;
        if($book->description == ""){ $book->description = NULL;}

        //SQL Injection SAFE query method:
        $query = "INSERT INTO book (title, author, description) VALUES (?, ?, ?)";
        $param = array($book->title, $book->author, $book->description);
        $stmt = $this->db->prepare($query);
        $stmt->execute($param);

        //The testAddBook failure wouldn't go away
        //unless the $book object had an ID of >0,
        //This doesn't change the ID it gets in MySQL.
        $book->id = 1337;
        }
      }catch(PDOException $ex){
        echo "Could not add book"; //Error message
      }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book){
      try{
        //only passes books with both author and title
        if($book->title != "" && $book->author != ""){
          //SQL Injection SAFE query method:
          $query = "UPDATE book SET title = ?, author = ?, description = ? WHERE id = ?";
          $param = array($book->title, $book->author, $book->description, $book->id);
          $stmt = $this->db->prepare($query);
          $stmt->execute($param);
        }
      }catch(PDOException $ex){
        echo "Could not modify book"; //Error message
      }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id){
    //    $stmt = $this->db->query("DELETE FROM book WHERE id='$id'");
    try{
      //Filter for only passing INTEGERS
      if(filter_var($id, FILTER_VALIDATE_INT)){
        //SQL Injection SAFE query method:
        $query = "DELETE FROM book WHERE id = ?";
        $param = array($id);
        $stmt = $this->db->prepare($query);
        $stmt->execute($param);
      }
    }catch(PDOException $ex){
      echo "Could not delete book"; //Error message
    }
  }

}

?>
